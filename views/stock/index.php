<?php

use app\components\Constants;
use app\models\Stock;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Stock */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stock';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-index">

    <?php $this->params['page-buttons'][] = Html::a('Add Stock', ['create'], ['class' => 'btn btn-success pull-right'])?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'pharmacy_id',
            ['attribute'=>'drug_id', 'format'=>'raw',
                'filter'=> Yii::$app->controller->renderPartial('@app/views/drugs/_filter', [
                    'searchModel'=>$searchModel->drugSearch]),
                'value'=>function(Stock $stock){
                    return Html::a($stock->drug->name, ['view', 'id'=>$stock->id]);
            }],
            ['attribute'=>'measure', 'filter'=>Constants::measures(), 'value'=>function(Stock $stock){
                return Constants::measures()[$stock->measure];
            }],
            ['attribute'=>'quantity'],
            'unit_cost',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
