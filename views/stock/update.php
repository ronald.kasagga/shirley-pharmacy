<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */
$drug = $model->drug->name;

$this->title = 'Update Stock: ' . $drug;
$this->params['breadcrumbs'][] = ['label' => 'Stock', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $drug, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stock-update">

    <?= $this->render('_form', [
        'model' => $model,
        'drugs'=>$drugs,
    ]) ?>

</div>
