<?php

use app\components\Constants;
use app\models\Stock;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */

$this->title = $model->drug->name;
$this->params['breadcrumbs'][] = ['label' => 'Stock', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-view">

        <?php $this->params['page-buttons'][] = Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php $this->params['page-buttons'][] = Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'pharmacy_id',
            ['attribute'=>'drug_id', 'value'=>$model->drug->name],
            //'measure',
            ['attribute'=>'quantity', 'value'=>$model->quantity . ' ' . Constants::measures()[$model->measure]],
        ],
    ]) ?>

</div>
