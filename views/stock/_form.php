<?php

use app\components\Constants;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */
/* @var $form yii\widgets\ActiveForm */
/* @var $drugs array */
if($model->isNewRecord && $model->unit_cost == null)
    $model->unit_cost = 1000?>

<div class="stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php  //$form->field($model, 'pharmacy_id')->textInput() ?>

    <div class="col-md-12">
        <?= $form->field($model, 'drug_id')->dropDownList($drugs, ['class'=>'form-control']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'measure')->dropDownList(Constants::measures(), ['class'=>'form-control']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'quantity')->textInput(['type'=>'number', 'steps'=>'0.1']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'unit_cost')->textInput(['type'=>'number', 'steps'=>'50']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
