<?php/**
 * @var $theme \app\assets\themes\MaxiHealthAsset
*/?>
<!-- Service Section -->
<div id="service-section" class="container-fluid no-padding service-section">
    <!-- Container -->
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Service -->
            <div class="col-md-8 col-sm-12 col-xs-12 service">
                <div class="section-header">
                    <h3>Welcome to medical</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,  totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-block">
                            <div class="service-block-icon">
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/ambulance.png" alt="ambulance"/></i>
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/ambulance-white.png" alt="ambulance-white"/></i>
                            </div>
                            <div class="service-block-content">
                                <h3>Emergency services</h3>
                                <p>Dolor sit amet consecdi pisicing eliam sed do eiusmod tempornu.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-block">
                            <div class="service-block-icon">
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/heart-ic.png" alt="heart-ic"/></i>
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/heart-ic-white.png" alt="heart-ic-white"/></i>
                            </div>
                            <div class="service-block-content">
                                <h3>Qualified Doctors</h3>
                                <p>Dolor sit amet consecdi pisicing eliam sed do eiusmod tempornu.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-block">
                            <div class="service-block-icon">
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/phone.png" alt="phone"/></i>
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/phone-white.png" alt="phone-white"/></i>
                            </div>
                            <div class="service-block-content">
                                <h3>24/7 support</h3>
                                <p>Dolor sit amet consecdi pisicing eliam sed do eiusmod tempornu.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="service-block">
                            <div class="service-block-icon">
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/appoinment-latter.png" alt="appoinment-latter"/></i>
                                <i><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/appoinment-latter-white.png" alt="appoinment-latter-white"/></i>
                            </div>
                            <div class="service-block-content">
                                <h3>online appointment</h3>
                                <p>Dolor sit amet consecdi pisicing eliam sed do eiusmod tempornu.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Service /- -->
            <div class="col-md-4 col-sm-12 col-xs-12">
                <!-- Appointment Form -->
                <form class="appoinment-form">
                    <h3><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/appoinment.png" alt="appoinment"/>Appointment form</h3>
                    <div class="form-group col-md-12 no-padding">
                        <input type="text" id="your-name" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group col-md-12 no-padding">
                        <input type="email" id="email" class="form-control" placeholder="Email Address">
                    </div>
                    <div class="form-group input-group col-md-12 no-padding">
                        <div class="col-md-7 no-padding">
                            <div class="col-md-6 col-sm-4 col-xs-4 no-left-padding">
                                <select class="form-control">
                                    <option>Day</option>
                                    <option>Sunday</option>
                                    <option>Monday</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-4 col-xs-4 no-left-padding">
                                <select class="form-control">
                                    <option>Time</option>
                                    <option>AM</option>
                                    <option>PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-4 no-padding">
                            <select class="form-control">
                                <option>Doctor Name</option>
                                <option>Mr.XYZ</option>
                                <option>Mr.ABC</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 no-padding">
                        <textarea rows="4" id="textarea_message" class="form-control" placeholder="Your Message..."></textarea>
                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 no-padding">
                        <input type="checkbox"><span>Send Email Notification</span>
                        <button type="submit" id="btn_submit" class="btn-submit pull-right">
                            <img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/heart-sm.png" alt="heart-sm">Submit</button>
                    </div>
                </form><!-- Appointment Form /- -->
            </div>
        </div>
    </div><!-- Container /- -->
</div><!-- Service Section /- -->

<!-- Latest News -->
<div class="container-fluid no-padding latest-news">
    <!-- Container -->
    <div class="container">
        <!-- Section Header -->
        <div class="section-header">
            <h3>Recent tips & News</h3>
            <a href="#">view More Post<i class="fa fa-plus"></i></a>
        </div><!-- Section Header /- -->
        <div class="row">
            <article class="col-md-6 col-sm-12 col-xs-12">
                <div class="entry-header">
                    <div class="entry-cover">
                        <a href="blog-post.html"><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/latest-news-1.jpg" alt="latest-news"/></a>
                        <a href="blog-post.html" class="read-more"><i class="fa fa-link"></i>Read More</a>
                    </div>
                </div>
                <div class="entry-content">
                    <div class="entry-meta">
                        <a href="#"><i class="fa fa-comment-o"></i>Comments<span>(12)</span></a>
                        <a href="#"><i class="fa fa-heart-o"></i>Favorite<span>(11)</span></a>
                        <a href="#"><i class="fa fa-share-alt"></i>Share Post<span>(12)</span></a>
                    </div>
                    <h3 class="entry-title"><a href="blog-post.html">Latest Blog new Slider Image Post</a></h3>
                    <div class="post-meta">
                        <a href="#" title="25th sep 2015" class="post-date">25th sep 2015</a> by
                        <a href="#" title="Mathov" class="post-admin">Mathov</a> in Hospital
                    </div>
                    <p>Voluptatem accusantium dolormque laudantium sa tota rem aperiam, eaque ipsa dicta sunt explicabo nemo enim ipsam [...] </p>
                </div>
            </article>
            <article class="col-md-6 col-sm-12 col-xs-12">
                <div class="entry-header">
                    <div class="entry-cover">
                        <a href="blog-post.html"><img src="http://localhost/shirley-pharmacy/web/assets/e66cb562/web/images/latest-news-2.jpg" alt="latest-news"/></a>
                        <a href="blog-post.html" class="read-more"><i class="fa fa-link"></i>Read More</a>
                    </div>
                </div>
                <div class="entry-content">
                    <div class="entry-meta">
                        <a href="#"><i class="fa fa-comment-o"></i>Comments<span>(18)</span></a>
                        <a href="#"><i class="fa fa-heart-o"></i>Favorite<span>(16)</span></a>
                        <a href="#"><i class="fa fa-share-alt"></i>Share Post<span>(13)</span></a>
                    </div>
                    <h3 class="entry-title"><a href="blog-post.html">Plan the most effective strategy</a></h3>
                    <div class="post-meta">
                        <a href="#" title="30th sep 2015" class="post-date">30th sep 2015</a> by
                        <a href="#" title="Mathov" class="post-admin">Mathov</a> in Hospital
                    </div>
                    <p>Voluptatem accusantium dolormque laudantium sa tota rem aperiam, eaque ipsa dicta sunt explicabo nemo enim ipsam [...] </p>
                </div>
            </article>
        </div>
    </div><!-- Container /- -->
</div><!-- Latest News /- -->