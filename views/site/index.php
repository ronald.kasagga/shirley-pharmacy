<?php
/**
 * @var $this \yii\base\View
*/
?>
<div id="service-section" class="container-fluid no-padding service-section">
    <!-- Container -->
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Service -->
            <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12 service">
                <div class="section-header">
                    <h3 class="text-center" style="background-image: none">Welcome to <?=Yii::$app->name?></h3>
                    <p style="padding: 0">
                        We are an online service that allows you to search for drugs and find them in a pharmacy near
                        you that has them in a stock level that fits your needs. Get started by searching for
                        a drug below.
                    </p>
                    <?=$this->renderFile('@app/views/drugs/_search_horizontal.php')?>
                </div>
            </div><!-- Service /- -->
        </div>
    </div><!-- Container /- -->
</div><!-- Service Section /- -->

