<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\themes\MaxiHealthAsset;
use app\components\Listings;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$theme = MaxiHealthAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body  data-offset="200" data-spy="scroll" data-target=".ow-navigation">
    <!-- Loader -->
    <a id="top"></a>
    <?php $this->beginBody() ?>

    <?php
    $menu = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Drugs', 'url' => ['/drugs'], 'active'=>('drugs' == Yii::$app->controller->id)],
        ['label' => 'Pharmacies', 'url' => ['/pharmacies'], 'active'=>('pharmacies' == Yii::$app->controller->id)],
    ];
    if(Yii::$app->user->isGuest){
        $menu[] = ['label' => 'Signup', 'url' => ['/user/register']];
        $menu[] = ['label' => 'Login', 'url' => ['/user/login']];
    }else{
        $menu[] = [
            'label' => 'Users',
            'items' => [
                ['label'=>'Admin', 'url'=>['/user/admin']],
                '<li class="divider"></li>',
                ['label'=>'Dev Options', 'url'=>['/user']],
            ]
        ];
        $menu[] = [
            'label' => 'Hello ' . Yii::$app->user->displayName,
            'items'=> [
                ['label'=>'Account', 'url' => ['/user/account']],
                ['label'=>'Profile', 'url' => ['/user/profile']],
                '<li class="divider"></li>',
                ['label'=>'Connect Facebook', 'url' => ['/user/auth/connect', 'authclient'=>'facebook']],
                '<li class="divider"></li>',
                ['label'=>'Log Out', 'url' => ['/user/logout'], 'linkOptions' => ['data-method' => 'post']]
            ],
        ];
    }

    ?>


    <div class="main-container">
        <?=$theme->header(Yii::$app->name, Yii::$app->homeUrl, [
            'mainView'=>$this,
            'items' => $menu,
        ])?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>


        <?=$theme->footer([
            'featuredCategories'=>Listings::featuredCategories(14),
        ])?>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
