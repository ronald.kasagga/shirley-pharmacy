<?php

use app\components\Constants;
use app\components\Listings;
use app\models\Stock;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drug */
/* @var $stockSearchModel app\models\search\Stock */
/* @var $stockDataProvider yii\data\ActiveDataProvider */
/* @var $subscriber \app\models\Subscriber */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Drugs', 'url' => ['search']];
$this->params['breadcrumbs'][] = $this->title;
$categories = Listings::drugCategories($model);
$noCategories = count($categories);
$suffix = $noCategories == 1  ? 'category ' : ' categories; ';
$catList = $noCategories == 0
    ? '' : " {$model->name} is in the $suffix".implode(', ', $categories).'. '?>

<div class="drug-view">

    <h1 class="col-md-12">
        <?= Html::a('&lt; Back to Search', ['drugs/search'], ['class'=>'btn btn-default pull-right'])?>
        <?= Html::encode($this->title) ?>
    </h1>

    <div>
        <div class="col-md-3"><?=Html::img($model->image)?></div>
        <div class="col-md-9">
            <?=trim($model->description. $catList.
                ' Visit '.Html::a($model->reference, $model->reference)." to learn more about {$model->name}.")?>
            <br><br>

            <?=$this->render('@app/views/drugs/_subscribe', [
                'drug'=>$model,
                'subscriber'=>$subscriber
            ])?>
            <?=$this->render('@app/views/categories/_list', ['categories'=>$model->categories])?>
        </div>
    </div>
    <div class="col-md-12">

        <div class="row col-lg-12">
            <?= Yii::$app->controller->renderPartial('@app/views/pharmacies/_pharmacy_location_search_form', [
                'searchModel'=>$stockSearchModel->pharmacySearch])?>
        </div>

        <h2>Pharmacies with <?=$model->name?></h2>

        <?php if($stockSearchModel->pharmacySearch->location != null):?>
            <h4 class="text-success pull-right col-xs-12"><i class="fa fa-map-marker"></i> Near
                <?=$stockSearchModel->pharmacySearch->location?>
            </h4>
        <?php endif;?>

        <?= GridView::widget([
            'dataProvider' => $stockDataProvider,
            'filterModel' => $stockSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                ['attribute'=>'pharmacy_id', 'format'=>'raw',
                    'filter'=>Yii::$app->controller->renderPartial('@app/views/pharmacies/_filter', [
                        'searchModel'=>$stockSearchModel->pharmacySearch]),
                    'value'=>function(Stock $stock){
                            return Html::a($stock->pharmacy->name, ['/pharmacies/info',
                                'id'=>$stock->pharmacy_id,
                                'Drug[name]'=>$stock->drug->name
                            ]);
                    }
                ],
                ['label'=>'Location', 'value'=>'pharmacy.location'],
                ['label'=>'Distance', 'value'=>'pharmacy.searchedLocationDistance'],
                ['label'=>'In Stock', 'value'=>function(Stock $stock){
                    return $stock->quantity.' '. Constants::measures()[$stock->measure];
                }],
                'unit_cost'
            ],
        ]); ?>
    </div>

</div>
