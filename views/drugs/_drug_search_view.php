<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drug */

?>
<div class="drug-view">

    <div class="panel">
        <div class="panel-heading col-md-12">
            <h1 class="panel-title">
                <?= Html::a(Html::encode($model->name), ['info', 'id'=>$model->id], ['style'=>'font-size:20px']) ?>
            </h1>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <?=Html::img($model->image, ['style'=>'max-height:250px', 'class'=>'img-responsive '])?>
                </div>
                <div class="col-md-9">
                    <?php if(isset($model->description)):?>
                        <p><?=$model->description?></p>
                    <?php endif;?>

                    <?=$this->render('@app/views/categories/_list', ['categories'=>$model->categories])?>
                    <hr>
                    <div class="">
                        <a href="<?=Url::to(['info', 'id'=>$model->id])?>"><i class="fa fa-search"></i> Find Pharmacy</a> |
                        <a href="<?=$model->reference?>"><i class="fa fa-globe"></i> More Info</a>
                    </div>
                </div>
                <!--<span class="row pull-right">Reference: <?/*=Html::a('druginfo.nlm.nih.gov', $model->reference)*/?></span>-->
            </div>

        </div>
    </div>

</div>
