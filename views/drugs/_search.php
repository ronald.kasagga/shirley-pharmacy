<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\Drug */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drug-search">

    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'method' => 'get',
    ]); ?>

    <div class="input-group">
        <?= Html::activeInput('text', $model, 'name', ['class'=>'form-control']) ?>

        <div class="input-group-btn">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default', 'onclick'=>'window.location=\''.Url::to(['/drugs']).'\'']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
