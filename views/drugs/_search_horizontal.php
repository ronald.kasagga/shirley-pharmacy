<?php
/**
 * 
 */
use yii\helpers\Url; ?>
<form action="<?=Url::to(['/drugs'])?>" class="appoinment-form form-horizontal" style="border: none">
    <div class="input-group">
        <input name="Drug[name]" type="text" class="form-control" placeholder="Enter drug/illness e.g. amoxil, flu, cough ...">
        <span class="input-group-btn">
            <button type="submit" class="btn-submit" style="margin-top: 3px; max-height: 38px">
            <i class="fa fa-search-plus"></i> Search</button>
        </span>
    </div>
</form>
