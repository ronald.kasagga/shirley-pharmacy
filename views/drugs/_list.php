<?php
/**
 * @var $drugs \app\models\Drug[]
 */
use yii\helpers\Html;
$total = count($drugs);
$suffix = $total == 1 ? '' : 's'?>

<?php if(!empty($drugs)):?>
    <br>
    <div class="col-md-12">
        <h4><?=$total.' drug'.$suffix?> in this category</h4>
        <ul class="">
            <?php foreach ($drugs as $drug)
                echo '<li style="list-style-type: none" class="col-md-3">' . ' &DoubleRightArrow; ' .
                    Html::a($drug->name, ['/drugs/info', 'id'=>$drug->id]) .
                    '</li>'?>
        </ul>
    </div>
<?php endif;?>
