<?php
use yii\helpers\Html;
/**
 * @var $searchModel \app\models\search\Drug
 */
?>

<div class="form-group">
    <?=Html::activeInput('text', $searchModel, 'name', ['class'=>'form-control'])?>
</div>
