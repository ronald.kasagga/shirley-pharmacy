<?php

use app\models\Drug;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Drug */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drug-index">

    <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
    <span class="hint-block text-center">Medicine or Health Conditions</span>
    <br>
    <div class="com-md-8">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <br>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'tag' => 'div',
            'class' => 'list-wrapper',
            'id' => 'list-wrapper',
        ],
        'layout' => "{pager}\n{summary}\n{items}\n{pager}",
        'itemView' => '_drug_search_view',
    ]); ?>
</div>
