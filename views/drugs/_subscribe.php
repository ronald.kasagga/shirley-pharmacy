<?php
use yii\helpers\Html;
use yii\helpers\Url;
/**
 * @var $this \yii\web\View
 * @var $drug \app\models\Drug
 * @var $subscriber \app\models\Subscriber
 */
$host = Yii::$app->params['server_name'];
?>

<p>Subscribe below to receive notification whenever <?=$drug->name?> is stocked in any of our partner pharmacies</p>

<div class="drug-subscribe-form">
    <?= Html::beginForm(Url::to(['/drugs/subscribe', 'id'=>$drug->id]), 'post', ['class'=>'appoinment-form form-horizontal']); ?>

    <div class="input-group">
        <div class="form-group col-md-6">
            <?= Html::activeInput('text', $subscriber, 'name', ['class'=>'form-control', 'placeholder'=>'John Smith'])?>
        </div>
        <div class="form-group col-md-6">
            <?= Html::activeInput('email', $subscriber, 'email', [
                'class'=>'form-control col-md-6', 'placeholder'=>"john.smith@$host"
        ])?>
        </div>
        <span class="input-group-btn">
            <?= Html::submitButton('Subscribe', ['class'=>'btn btn-submit'])?>
        </span>
    </div>

    <?= Html::endForm(); ?>
</div>

<style>
    form.appoinment-form {
        border: none
    }
    div.drug-subscribe-form .input-group .form-group {
        padding: 0;
    }
    div.drug-subscribe-form .input-group span.input-group-btn button{
        margin-left: -45px;
        margin-top: -14px;
        max-height: 38px
    }
</style>
