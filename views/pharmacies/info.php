<?php

use app\components\Constants;
use app\models\Stock;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pharmacy */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pharmacies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pharmacy-view">

       <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'name',
            'description:ntext',
            'location',
        ],
    ]) ?>
    <div class="col-md-12">
        <h3>Drugs we have in stock</h3>
        <?= GridView::widget([
            'dataProvider' => $stockDataProvider,
            'filterModel' => $stockSearchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'pharmacy_id',
                ['attribute'=>'drug_id', 'format'=>'raw',
                    'filter'=> Yii::$app->controller->renderPartial('@app/views/drugs/_filter', [
                        'searchModel'=>$stockSearchModel->drugSearch]),
                    'value'=>function(Stock $stock){
                        return Html::a($stock->drug->name, ['/drugs/info', 'id'=>$stock->drug_id]);
                    }],
                ['attribute'=>'measure', 'filter'=>Constants::measures(), 'value'=>function(Stock $stock){
                    return Constants::measures()[$stock->measure];
                }],
                ['attribute'=>'quantity'],
                'unit_cost'
            ],
        ]); ?>
    </div>

</div>
