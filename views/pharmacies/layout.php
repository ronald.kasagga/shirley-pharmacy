<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $content string
 */
$hasPharmacy = \app\models\User::hasPharmacy();
$menu = [['label' => '<i class="fa fa-search-plus"></i> Search',
            'url' => ['/pharmacies/index'], 'encode'=>false]];

if($hasPharmacy !== null){
     $pharmacyButtonLabel = (
        ($hasPharmacy == false)
            ? '<i class="glyphicon glyphicon-plus"></i> Create Pharmacy'
            : '<i class="fa fa-eye"></i> View Pharmacy'
     );
    $menu[] = ['label'=>$pharmacyButtonLabel, 'url'=>['pharmacies/create'], 'encode'=>false];
}
if($hasPharmacy == true){
    $menu[] = ['label'=>'<i class="fa fa-building"></i> Stock',
               'url'=>['/stock'], 'encode'=>false];
}

?>
<?php $this->beginContent('@app/views/layouts/main.php',[]); ?>
    <div class="col-md-12">
        <?=Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-pills pull-right'],
            'items' => $menu,
        ]);?>
        <h2 class="pull-left pharmacy-title-h2">
            <i class="fa fa-stethoscope"></i>
            <?= Html::encode($this->title) ?>
            <?php if(isset($this->params['page-buttons']) && !empty($this->params['page-buttons'])){
                foreach ($this->params['page-buttons'] as $button)
                    echo '&nbsp; '.$button;
            }?>
        </h2>
        <hr class="col-md-12 pharmacy-menu-hr">
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=$content?>
        </div>
    </div>
<?php $this->endContent()?>

<style>
    ul.breadcrumb{
        margin-bottom: 10px;
    }
    hr.pharmacy-menu-hr{
        margin-top: 10px;;
        margin-bottom: 10px;;
    }
    h2.pharmacy-title-h2{
        margin-top: 10px
    }
</style>
