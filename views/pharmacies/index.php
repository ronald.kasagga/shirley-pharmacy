<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Pharmacy */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $hasPharmacy bool */
/* @var $user \app\models\User*/

$this->title = 'Pharmacies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pharmacy-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute'=>'name', 'format'=>'raw', 'value'=>function(\app\models\Pharmacy $pharmacy){
                return Html::a($pharmacy->name, ['info', 'id'=>$pharmacy->id]);
            }],
            'description:ntext',
            'location',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
