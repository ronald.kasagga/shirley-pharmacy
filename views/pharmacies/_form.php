<?php
use app\assets\plugins\LocationPickerAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pharmacy */
/* @var $form yii\widgets\ActiveForm */

$mapConfig = [
    'mapDivId' => 'pharmacy-location-map',
    'initLocation' => [
        'latitude' => $model->latitude != null ? $model->latitude : 0.28591934803410723,
        'longitude' => $model->longitude != null ? $model->longitude : 32.60776791041599,
    ],
    'latitudeInputId' => Html::getInputId($model, 'latitude'),
    'longitudeInputId' => Html::getInputId($model, 'longitude'),
    'locationInputId' => Html::getInputId($model, 'location'),
    'zoom'=>18,
]?>

<div class="pharmacy-form">
    <?php LocationPickerAsset::register($this)->initialize($mapConfig, $this)?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 8]) ?>

            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <div id="pharmacy-location-map" class="col-md-12" style="width: 568px; height: 269px"></div>
            <div class="clearfix">&nbsp;</div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'latitude')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'longitude')->textInput() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
