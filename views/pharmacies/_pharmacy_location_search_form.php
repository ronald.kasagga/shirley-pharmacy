<?php
use app\assets\plugins\LocationPickerAsset;
use yii\helpers\Html;

/**
 * @var $searchModel \app\models\search\Pharmacy
 */

$latitude = $searchModel->latitude;
$longitude = $searchModel->longitude;

if(in_array(null, [$searchModel->latitude, $searchModel->longitude])){
    $latitude = 0;
    $longitude = 0;
    //set current location
}

$config = [
    'mapDivId' => 'invisible-pharmacy-map',
    'initLocation' => [
        'latitude' => $latitude,
        'longitude' => $longitude,
    ],
    'latitudeInputId' => Html::getInputId($searchModel, 'latitude'),
    'longitudeInputId' => Html::getInputId($searchModel, 'longitude'),
    'locationInputId' => Html::getInputId($searchModel, 'location'),
    'zoom'=>18,
]?>

<?php LocationPickerAsset::register($this)->initialize($config, $this)?>

<?= Html::beginForm(Yii::$app->request->url, 'get', ['class'=>'appoinment-form form-horizontal pharmacy-location-search-form']); ?>

    <div class="input-group col-md-12">
        <?=Html::activeInput('hidden', $searchModel, 'name', ['class'=>'form-control'])?>
        <?=Html::activeInput('hidden', $searchModel, 'latitude', ['class'=>'form-control'])?>
        <?=Html::activeInput('hidden', $searchModel, 'longitude', ['class'=>'form-control'])?>
        
        <?=Html::input('text', Html::getInputName($searchModel, 'location'), $searchModel->location, [
            'class'=>'form-control', 'id'=> Html::getInputId($searchModel, 'location')
        ])?>
        <span class="input-group-btn">
            <?= Html::submitButton('<i class="fa fa-map-marker"></i> Find', ['class'=>'btn btn-success'])?>
        </span>
    </div>

<?= Html::endForm(); ?>

<div style="width: 1000px; height: 100px; display: none" id="invisible-pharmacy-map"></div>

<style>
    .pharmacy-location-search-form button{
        margin-top: 3px;
        padding: 8px 26px;
    }
    .pharmacy-location-search-form span.input-group-addon{
        height:10px
    }

</style>
