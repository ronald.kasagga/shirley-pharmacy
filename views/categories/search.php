<?php

use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Category */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h2>Categories in our database</h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute'=>'name', 'format'=>'raw', 'value'=>function(Category $category){
                return Html::a($category->name, ['info', 'id'=>$category->id]);
            }],
            'description:ntext',
            //'reference',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
