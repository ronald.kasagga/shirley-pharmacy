<?php
/**
 * @var $categories \app\models\Category[]
 */
use yii\helpers\Html; ?>

<?php if(!empty($categories)):?>
    <br>
    <h4>Categories</h4>
    <ul class="">
        <?php foreach ($categories as $category)
            echo '<li style="list-style-type: none">' . ' &DoubleRightArrow; ' .
                Html::a($category->name, ['/categories/info', 'id'=>$category->id]) .
                '</li>'?>
    </ul>
<?php endif;?>
