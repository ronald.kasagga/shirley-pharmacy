<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['search']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <?=trim($model->description.' Visit '.Html::a($model->reference, $model->reference).' to learn more about this drug category.')?>

    <section class="row">
        <?=$this->render('@app/views/drugs/_list', ['drugs'=>$model->drugs])?>
        <br>
    </section>
    <p style="min-height: 20px"></p>

</div>
