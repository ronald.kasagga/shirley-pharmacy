<?php
namespace app\components;

use app\models\Drug;
use DOMDocument;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class MedicineFinder
{
    public static function drugInfo_nlm_nih_gov_drugs($verbose = false){
        $site = 'http://druginfo.nlm.nih.gov';
        $listRootUrl = '/drugportal/drug/names/';
        foreach (range('a', 'z') as $index){
            $url = $site.$listRootUrl.$index;
            echo "\nFetching from $url ...\n";
            $html = RemoteFiles::get($url, $verbose);
            if(!is_string($html)) continue;

            if($verbose) echo "\tParsing . . . ";
            $dom = new DOMDocument();
            $dom->loadHTML($html);
            $position = 0;
            foreach ($dom->getElementsByTagName('table') as $table){
                if(++$position == 3){ //#3rd table has drug list
                    $drugListTableHtml = $dom->saveHTML($table);
                    $drugDom = new DOMDocument();
                    $drugDom->loadHTML($drugListTableHtml);
                    foreach ($drugDom->getElementsByTagName('a') as $link){
                        if($verbose) echo "\t retrieved {$link->nodeValue}\n";
                        yield [
                            'name'=>$link->nodeValue,
                            'reference'=>$site.$link->getAttribute('href'),
                        ];
                    }                        
                }
            }
        }
    }

    public static function drugInfo_nlm_nih_gov_categories($verbose = false){
        $site = 'http://druginfo.nlm.nih.gov';
        $listRootUrl = '/drugportal/drug/categories/';
        foreach (range('a', 'w') as $index){
            $url = $site.$listRootUrl.$index;
            
            $siteSource = RemoteFiles::get($url, $verbose);
            if(!is_string($siteSource)) continue;

            if($verbose) echo "\tParsing . . . ";
            $html = '';
            foreach (preg_split("/((\r?\n)|(\r\n?))/", $siteSource) as $line){
                if(substr( $line, 0, 28 ) === '<tr><td><div class="cat-list'){
                    $html = $line;
                    break;
                }
            }

            $dom = new DOMDocument('1.0', 'UTF-8');
            
            // set error level
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($html);

            foreach ($dom->getElementsByTagName('p') as $paragraph){
                $dataCount = $paragraph->getAttribute('data-count');
                if($dataCount > 1){
                    $category = [];
                    
                    $catHtml =  $dom->saveHTML($paragraph);
                    $catDom = new DOMDocument();
                    $catDom->loadHTML($catHtml);
                    foreach ($catDom->getElementsByTagName('a') as $catLink){
                        $category['name'] = $catLink->nodeValue;
                        $category['reference'] = $site.$catLink->getAttribute('href');
                        break;
                    }
                    foreach ($catDom->getElementsByTagName('span') as $catSpan)
                        $category['description'] = trim(str_replace(' MeSH', '', preg_replace('/[^\da-z ]/i', '', $catSpan->nodeValue)));
                    
                    yield $category;
                }
            }
            
            //restore error level
            libxml_use_internal_errors($internalErrors);
        }
    }

    public static function drugInfo_nlm_nih_gov_drug_detail(Drug $drug, $verbose = false){
        $html = RemoteFiles::get($drug->reference, $verbose);
        if($html == false) return [];

        $data = $drug->attributes;
        $foundTable = false;
        $foundDesc = false;
        $table = '';
        $desc = null;
        
        if($verbose) echo "\tParsing html ...\n";
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $html) as $line){
            $phrase = trim($line);
            if($foundTable){
                $table .= $phrase."\n";
                if($phrase == '</table>')
                    break;

                if($foundDesc){
                    $desc = $phrase;
                    $foundDesc = false;
                }

                if($phrase == '<td class="label">Description:</td>')
                    $foundDesc = true;

            } else if($phrase == '<table class="search-results" width="100%">'){
                $foundTable = true;
                $table .= $phrase."\n";
            }
        }

        if(!$foundTable) return $data;
        
        if($desc != null){
            if($verbose) echo "\tFound description ...\n";
            
            $description = str_replace('<td colspan="2">', '', $desc);
            $description = str_replace('</td>', '', $description);
            $data['description'] = html_entity_decode($description);/*
            if(StringHelper::startsWith($desc, '<td', false))
                $data['description'] = $desc;
            else{
                $description = html_entity_decode($desc);
                $description = str_replace('&', '$and$', $description);
                $dom = new DOMDocument();
                $dom->loadHTML(html_entity_decode($description));
                foreach ($dom->getElementsByTagName('td') as $td){
                    $description = str_replace('$and$', '&', $td->nodeValue);
                    $data['description'] = preg_replace('/[^\da-z\/ ]/i', '', str_replace('$and$', '&', $description));
                }
            }
            return $data['description'];*/
        }

        $list = '';
        $foundList = false;
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $table) as $line){
            if($foundList){
                $list .= $line.' ';
                if(strpos($line, '</li>'))
                    break;
            } else if(substr( $line, 0, 30 ) === '<li class="cat-toggle hidden">'){
                $foundList = true;
                $list .= $line;
            }
        }
        if(!$foundList) return $data;

        $ul = str_replace('ahref', 'a href', html_entity_decode($list));
        $ul = str_replace('</a >', '</a>', $ul);

        $dom = new DOMDocument();
        $dom->loadHTML($ul);
        $data['categories'] = [];
        
        if($verbose) echo "\tParsing categories ...\n";
        foreach ($dom->getElementsByTagName('a') as $a){
            $category = $a->nodeValue;
            if(!strpos($category, 'Info')){
                if(!strpos($category, ',')) $data['categories'][] = $category;
                else{
                    $options = explode(',', $category);
                    foreach ($options as $option){
                        $option = str_replace(' and ', '', $option);
                        $data['categories'][] = trim($option);
                    }
                }
                if($verbose) echo "\tFound ".count($data['categories'])." categories \n";
            }
        }

        return $data;
    }
}