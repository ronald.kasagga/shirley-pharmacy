<?php

namespace app\components;


class Calculator
{
    const EARTH_RADIUS = 6371000;
    
    public static function getDistance($from, $to, $format = 'km', $accuracy=3){
        
        if(empty($from) || empty($to))
            return null;
        
        foreach (['latitude', 'longitude'] as $key)
            if(!isset($from[$key]) || !isset($to[$key]))
                return null;
        
        $distance =  self::vincentyGreatCircleDistance($from, $to);
        switch ($format){
            case 'km':
                return round( (float) ($distance/1000), $accuracy);
            default:
                return $distance;
        }
    }

    private static function vincentyGreatCircleDistance($from, $to)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($from['latitude']);
        $lonFrom = deg2rad($from['longitude']);
        $latTo = deg2rad($to['latitude']);
        $lonTo = deg2rad($to['longitude']);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return $angle * self::EARTH_RADIUS;
    }

    private static function haversineGreatCircleDistance($from, $to)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($from['latitude']);
        $lonFrom = deg2rad($from['longitude']);
        $latTo = deg2rad($to['latitude']);
        $lonTo = deg2rad($to['longitude']);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * self::EARTH_RADIUS;
    }
}