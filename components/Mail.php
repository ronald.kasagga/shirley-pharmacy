<?php

namespace app\components;

use app\models\Drug;
use app\models\Stock;
use app\models\Subscriber;
use Yii;

class Mail
{
    public static function sendSubscriptionEmail(Drug $drug, Subscriber $subscriber)
    {
        $app_name = Yii::$app->name;
        return
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['business-email'])
                ->setTo($subscriber->email)
                ->setSubject("$app_name: Subscription to {$drug->name}")
                ->setHtmlBody(self::renderer('@app/mail/bodies/_drugSubscription', [
                    'drug'=>$drug,
                    'subscriber'=>$subscriber,
                ]))->send();
    }
    
    public static function renderer($view, $params = []){
        return Yii::$app->controller->renderPartial($view, $params);
    }

    public static function sendStockChangeEmail(Stock $stock)
    {
        $drug = $stock->drug;
        $subscriptions = $drug->subscriptions;
        $app_name = Yii::$app->name;
        
        if(!empty($subscriptions)){
            $pharmacy = $stock->pharmacy;
            foreach ($subscriptions as $subscription){
                Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['business-email'])
                    ->setTo($subscription->email)
                    ->setSubject("$app_name: {$drug->name} Now Available")
                    ->setHtmlBody(self::renderer('@app/mail/bodies/_drugStockNotification', [
                        'drug'=>$drug,
                        'subscription'=>$subscription,
                        'pharmacy'=>$pharmacy
                    ]))->send();
            }
        }
    }
}