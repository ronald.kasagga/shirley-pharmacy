<?php

namespace app\components;

use yii\helpers\Url;

class DataMocker
{

    public static function randomImageUrl(){
        $i = rand(1,12);
        return Url::to(["/img/medicine/meds ($i).jpg"]);
    }
}