<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/13/2016
 * Time: 7:16 PM
 */

namespace app\components;


class Constants
{
    private static $_constants;
    const SEARCHED_PHARMACY_COORDINATES = 'searched_pharmacy_coordinates';

    public static function measures(){
        return [
            'g'=>'Grams',
            'mg'=>'Milligrams',
            'tab'=>'Tablets',
            'btl'=>'Bottles',
        ];
    }
    
    public static function setConstant($key, $value){
        if(self::$_constants == null)
            self::$_constants = [];
        
        self::$_constants[$key] = $value;
    }
    
    public static function getConstant($key){
        return isset(self::$_constants[$key])
            ? self::$_constants[$key] : null;
    }
}