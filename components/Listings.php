<?php

namespace app\components;


use app\models\Drug;
use yii\helpers\Html;

class Listings
{
    public static function featuredCategories($limit=12)
    {

        $categoryNames = ['Anesthetics','Antacids',
            'Antibiotics','Antidotes','Antidiarrheals',
            'Cosmetics','Disinfectants','Detergents','Estrogens','Food Additives',
            'Hormones','Insecticides','Narcotics','Neurotoxins','Oxidants',
            'Vaccines','Vitamins','Solvents'
        ];

        $categories = '"'.implode('","', $categoryNames).'"';
        $query = "select c.* from category c where c.name in ($categories) order by rand() limit $limit";
        $links = [];

        $records = \Yii::$app->db->createCommand($query)->queryAll();
        foreach ($records as $category)
            $links[] = Html::a($category['name'], ['/categories/info', 'id'=>$category['id']]);

        return $links;
    }
    
    public static function drugCategories(Drug $drug){
        $categories = $drug->categories;
        if(empty($categories)) return [];

        $links = [];
        foreach ($categories as $category)
            $links[] = Html::a($category->name, ['/categories/info', 'id'=>$category->id]);

        return $links;
    }
}