<?php

namespace app\components;


class RemoteFiles
{
    public static function get($url, $verbose = false, $tries = 5){
        $try = 0;
        while ($try <= $tries){
            $try++;
            if($verbose) echo "\tTry $try: fetching from $url\n";
            $content = @file_get_contents($url);
            if($content === FALSE){
                $wait = rand(2, $tries);
                if($verbose) echo "\tWaiting $wait seconds . . .\n";
                sleep($wait);
            }
            else
                return $content;
        }
        $error = "Failed to open $url after $tries tries. Gave up at " . date('Y-m-d h:i:s');
        echo "\n\033[01;31m $error \033[0m";
        return false;
    }
}