<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $drug \app\models\Drug
 * @var $subscriber \app\models\Subscriber
 */
$homeUrl = Html::a(Yii::$app->name, Url::home(true))?>

<div>
    
    <p>Hello <?=$subscriber->name?>,</p>

    <p>
        You recently subscribed to receive notifications on the drug
        <?=Html::a($drug->name, Url::to(['/drugs/info', 'id'=>$drug->id], true))?> 
        whenever any one of our partner pharmacies at <?=$homeUrl?> updates their stock.
    </p>
    
    <p>This is a courtesy email informing you that you will indeed get notified whenever the stock is updated</p>
    <br><hr>
    
    <p>thanks,</p>
    <p>The <?=$homeUrl?> Team </p>
    
</div>
