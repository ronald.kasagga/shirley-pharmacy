<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $drug \app\models\Drug
 * @var $subscription \app\models\DrugSubscription
 * @var $pharmacy \app\models\Pharmacy
 */
$homeUrl = Html::a(Yii::$app->name, Url::home(true));
$pharmacy_link = Html::a($pharmacy->name, Url::to(['/pharmacies/info', 'id'=>$pharmacy->id], true));
$detail_url = Url::to(['/drugs/info', 'id'=>$drug->id, 'Stock[pharmacy_id]'=>$pharmacy->id], true)?>

<div>
    
    <p>Hello <?=$subscription->name?>,</p>

    <p>
        We would like to inform you that new stock of
        <?=Html::a($drug->name, Url::to(['/drugs/info', 'id'=>$drug->id], true))?>
        is available at one of our partner pharmacies <?=$pharmacy_link?>. Click
        <?=Html::a('here', $detail_url)?> for more detail.
    </p>

    <br><hr>
    
    <p>thanks,</p>
    <p>The <?=$homeUrl?> Team </p>
    
</div>
