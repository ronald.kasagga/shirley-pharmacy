<?php

namespace app\commands;
use app\components\MedicineFinder;
use app\models\Category;
use app\models\Drug;
use yii\console\Controller;

class MedicineController extends Controller
{
    public function actionGetDrugs($verbose=1){
        $verbose = ($verbose==1)?true:false;
        foreach (MedicineFinder::drugInfo_nlm_nih_gov_drugs($verbose) as $drugData){
            $drug = new Drug();
            $drug->attributes = $drugData;
            if($verbose) echo "\t saving {$drug->name}\n";
            $drug->save();
        }            
    }

    public function actionGetCategories($verbose=1){
        $verbose = ($verbose==1)?true:false;
        foreach (MedicineFinder::drugInfo_nlm_nih_gov_categories($verbose) as $categoryData){
            $category = new Category();
            $category->attributes = $categoryData;
            if($verbose) echo "\t saving {$category->name}\n";
            $category->save();
        }
    }

    public function actionUpdateDrugProperties($verbose = 1){
        echo "\nUpdating Drug Properties\n\n";

        $verbose = ($verbose==1)?true:false;
        $log_dir = \Yii::getAlias('@app/runtime/medicine/');
        $log_file = $log_dir.'drug_last_id';
        $last_id = 0;
        $total = Drug::find()->count();
        if($verbose) echo "::Found $total drugs in database\n";

        if(!file_exists($log_dir)){
            if($verbose) echo "::creating log directory $log_dir\n";
            mkdir($log_dir);
        }else if(file_exists($log_file)){
            $last_id = (int) file_get_contents($log_file);
            if($verbose) echo "::last processed id $last_id\n";
        }

        if($verbose) {
            if($last_id != 0) echo "\n::Continuing processing . . .\n";
            else echo "\n::Starting processing . . .\n";
        }

        for($id = ($last_id + 1); $id <= $total; $id += 1){
            $drug = Drug::findOne($id);
            if($drug != null){
                if($verbose) echo "\nprocessing {$id}/{$total}: {$drug->name}\n";
                $detail = MedicineFinder::drugInfo_nlm_nih_gov_drug_detail($drug, $verbose);
                if(empty($detail)) continue;

                if(isset($detail['description']) && $detail['description'] != null){
                    $drug->description = $detail['description'];
                    $drug->save();
                }

                if(isset($detail['categories']) && !empty($detail['categories'])){
                    $categories = Category::find()->where(['name'=>$detail['categories']])->all();
                    foreach ($categories as $category)
                        $drug->link('categories', $category);
                }

                $last_id++;
                file_put_contents($log_file, $id);
                sleep(3);
            }
        }

        if($last_id >= $total){
            echo "\nComplete ! !\n";
            unlink($log_file);
            unlink($log_dir);
        }
    }
    
    public function actionExport(){
        $tables = [Drug::tableName(), Category::tableName(), 'drug_category'];
        $data_folder = \Yii::getAlias('@app/data');
        if(!file_exists($data_folder))
            mkdir($data_folder, 0777, true);

        echo "\nstarting export ...\n";
        
        foreach ($tables as $table){
            echo "\nProcessing $table...";
            $filename = $data_folder."/{$table}.json";
            $data = \Yii::$app->db->createCommand("select * from $table")->queryAll();
            file_put_contents($filename, json_encode($data));
            echo 'complete';
        }
        
        echo "\nfinished export";
    }
    
    public function actionImport(){
        $tables = [Drug::tableName(), Category::tableName(), 'drug_category'];
        $data_folder = \Yii::getAlias('@app/data');
        if(!file_exists($data_folder)){
            echo 'export directory does not exist';
            exit;
        }
        
        foreach ($tables as $table){
            echo "\nProcessing $table...";
            $filename = $data_folder."/{$table}.json";
            $data =  json_decode(file_get_contents($filename));
            $index = 0; $total = count($data);
            foreach ($data as $row){
                $columns = (array) $row;
                echo "\n\tinserting $table [".(++$index)."/$total]";
                \Yii::$app->db->createCommand()->insert($table, $columns)->execute();                
            }
        }
        
        echo  "\nCompleted";
    }
}