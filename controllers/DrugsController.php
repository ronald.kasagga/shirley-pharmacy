<?php

namespace app\controllers;

use app\components\Mail;
use app\models\DrugSubscription;
use app\models\Subscriber;
use Yii;
use app\models\Drug;
use app\models\search\Drug as DrugSearch;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\search\Stock as StockSearch;
use yii\web\Response;

/**
 * DrugsController implements the CRUD actions for Drug model.
 */
class DrugsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $defaultAction = 'search';

    /**
     * Lists all Drug models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DrugSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch()
    {
        $searchModel = new DrugSearch();
        $dataProvider = $searchModel->nameSearch(Yii::$app->request->queryParams);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Drug model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionInfo($id)
    {
        $searchModel = new StockSearch();
        $params = Yii::$app->request->queryParams;
        $params['Stock']['drug_id'] = $id;
        $dataProvider = $searchModel->search($params);

        return $this->render('info', [
            'subscriber'=>new Subscriber(),
            'model' => $this->findModel($id),
            'stockSearchModel' => $searchModel,
            'stockDataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Drug model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Drug();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Drug model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Drug model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Drug model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Drug the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drug::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSubscribe($id){
        $drug = $this->findModel($id);
        $subscriber = new Subscriber();
        
        if(($subscriber->load(Yii::$app->request->post())) && $subscriber->validate()){
            $subscription = DrugSubscription::find()
                ->where(['drug_id'=>$id, 'email'=>$subscriber->email])->one();
            if($subscription == null){
                $subscription = new DrugSubscription();
                $subscription->attributes = $subscriber->attributes;
                $subscription->drug_id = $id;
                if($subscription->save())
                    Mail::sendSubscriptionEmail($drug, $subscriber);
            }
        }

        $this->redirect(['/drugs/info', 'id'=>$id]);

    }
}
