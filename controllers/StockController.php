<?php

namespace app\controllers;

use app\models\Drug;
use app\models\Pharmacy;
use app\models\User;
use Yii;
use app\models\Stock;
use app\models\search\Stock as StockSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StockController implements the CRUD actions for Stock model.
 * @property Pharmacy $pharmacy
 * @property array _drugs
 */
class StockController extends Controller
{

    private $_pharmacy;
    private $_drugs;
    public $layout = '@app/views/pharmacies/layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockSearch();
        $params = Yii::$app->request->queryParams;
        $params['Stock']['pharmacy_id'] = $this->pharmacy->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stock();
        $model->pharmacy_id = $this->pharmacy->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'drugs'=>$this->drugs,
            ]);
        }
    }

    /**
     * Updates an existing Stock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'drugs'=>$this->drugs,
            ]);
        }
    }

    /**
     * Deletes an existing Stock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return \app\models\Pharmacy|\yii\web\Response
     */
    public function getPharmacy(){
        if($this->_pharmacy == null){
            if(($user = User::getCurrent()) == null || $user->pharmacy_id == null)
                return $this->redirect(Url::home());

            $this->_pharmacy = $user->pharmacy;
        }

        return $this->_pharmacy;
    }

    public function getDrugs(){
        if($this->_drugs == null){
            $drugs = Drug::find()->all();
            $this->_drugs = empty($drugs)
                ? []
                : ArrayHelper::map($drugs, 'id', 'name');
        }
        return $this->_drugs;
    }
}
