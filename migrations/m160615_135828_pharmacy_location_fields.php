<?php

use yii\db\Migration;
use yii\db\Schema;

class m160615_135828_pharmacy_location_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%pharmacy}}', 'latitude', Schema::TYPE_STRING);
        $this->addColumn('{{%pharmacy}}', 'longitude', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%pharmacy}}', 'latitude');
        $this->dropColumn('{{%pharmacy}}', 'longitude');
    }
}
