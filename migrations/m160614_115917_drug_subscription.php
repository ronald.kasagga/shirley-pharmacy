<?php

use yii\db\Migration;
use yii\db\Schema;

class m160614_115917_drug_subscription extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%drug_subscription}}',[
            'id'=>Schema::TYPE_PK,
            'drug_id'=>Schema::TYPE_INTEGER,
            'name'=>Schema::TYPE_STRING,
            'email'=>Schema::TYPE_STRING . ' NOT NULL',
        ]);
        
        $this->addForeignKey('fk_drug_subscription_drug_id', '{{%drug_subscription}}', 'drug_id', '{{%drug}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_drug_subscription_drug_id', '{{%drug_subscription}}');
        $this->dropTable('{{%drug_subscription}}');
    }
}
