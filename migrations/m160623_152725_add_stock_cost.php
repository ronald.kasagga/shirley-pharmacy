<?php

use yii\db\Migration;
use yii\db\Schema;

class m160623_152725_add_stock_cost extends Migration
{
    public function safeUp()
    {
        $defaultUnitCost = 1000;
        $this->addColumn('{{%pharmacy_drug_stock}}', 'unit_cost', Schema::TYPE_DOUBLE." not null default $defaultUnitCost");
        $this->update('{{%pharmacy_drug_stock}}', ['unit_cost'=>$defaultUnitCost]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%pharmacy_drug_stock}}', 'unit_cost');
    }
}
