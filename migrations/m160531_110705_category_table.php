<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_110705_category_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING,
            'description'=>Schema::TYPE_TEXT,
            'reference'=>Schema::TYPE_STRING,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
