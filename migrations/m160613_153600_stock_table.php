<?php

use yii\db\Migration;
use yii\db\Schema;

class m160613_153600_stock_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%pharmacy_drug_stock}}', [
            'id'=>Schema::TYPE_PK,
            'pharmacy_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'drug_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'measure'=>Schema::TYPE_STRING . ' NOT NULL',
            'quantity'=>Schema::TYPE_DOUBLE . ' NOT NULL',
        ]);
        
        $this->addForeignKey('fk_stock_pharmacy_id', '{{%pharmacy_drug_stock}}', 'pharmacy_id', 'pharmacy', 'id');
        $this->addForeignKey('fk_stock_drug_id', '{{%pharmacy_drug_stock}}', 'drug_id', 'drug', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%pharmacy_drug_stock}}');
    }
}
