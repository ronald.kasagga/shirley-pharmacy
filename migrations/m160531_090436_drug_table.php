<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_090436_drug_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%drug}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING,
            'description'=>Schema::TYPE_TEXT,
            'reference'=>Schema::TYPE_STRING,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%drug}}');
    }
}
