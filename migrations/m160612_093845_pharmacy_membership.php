<?php

use yii\db\Migration;

class m160612_093845_pharmacy_membership extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'pharmacy_id', \yii\db\Schema::TYPE_INTEGER);
        $this->addForeignKey('fk_user_pharmacy_id', 'user', 'pharmacy_id', 'pharmacy', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_pharmacy', 'user');
        $this->dropColumn('user', 'pharmacy_id');
    }
}
