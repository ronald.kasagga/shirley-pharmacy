<?php

use yii\db\Migration;
use yii\db\Schema;

class m160611_222336_pharmacy_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%pharmacy}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING,
            'description'=>Schema::TYPE_TEXT,
            'location'=>Schema::TYPE_STRING
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%pharmacy}}');
    }
}
