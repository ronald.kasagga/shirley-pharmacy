<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_142549_drug_category_table extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('{{%drug_category}}', [
            'drug_id'=>Schema::TYPE_INTEGER,
            'category_id'=>Schema::TYPE_INTEGER,
        ]);
        $this->addForeignKey('fk_drug_id', '{{%drug_category}}', 'drug_id', 'drug', 'id');
        $this->addForeignKey('fk_category_id', '{{%drug_category}}', 'category_id', 'category', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_category_id', '{{%drug_category}}');
        $this->dropForeignKey('fk_drug_id', '{{%drug_category}}');
        $this->dropTable('{{%drug_category}}');
    }    
}
