<?php

namespace app\models;

use app\components\Mail;
use Yii;

/**
 * This is the model class for table "pharmacy_drug_stock".
 *
 * @property integer $id
 * @property integer $pharmacy_id
 * @property integer $drug_id
 * @property string $measure
 * @property double $quantity
 * @property double $unit_cost
 *
 * @property Drug $drug
 * @property Pharmacy $pharmacy
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pharmacy_drug_stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pharmacy_id', 'drug_id', 'measure', 'quantity'], 'required'],
            [['pharmacy_id', 'drug_id'], 'integer'],
            [['quantity'], 'number'],
            [['measure'], 'string', 'max' => 255],
            [['drug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drug::className(), 'targetAttribute' => ['drug_id' => 'id']],
            [['pharmacy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pharmacy::className(), 'targetAttribute' => ['pharmacy_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pharmacy_id' => 'Pharmacy',
            'drug_id' => 'Drug',
            'measure' => 'Measure',
            'quantity' => 'Quantity',
            'unit_cost' => 'Unit Cost (UGX)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrug()
    {
        return $this->hasOne(Drug::className(), ['id' => 'drug_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPharmacy()
    {
        return $this->hasOne(Pharmacy::className(), ['id' => 'pharmacy_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(!$insert){
            if(isset($changedAttributes['quantity'])){
                if($this->quantity > $changedAttributes['quantity'])
                    Mail::sendStockChangeEmail($this);
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
}
