<?php

namespace app\models;

use app\components\DataMocker;
use Yii;

/**
 * This is the model class for table "drug".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $reference
 *
 * @property Category[] $categories
 * @property Stock[] $stock
 * @property DrugSubscription[] $subscriptions
 * @property string image
 */
class Drug extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'reference' => 'Reference',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery|Category[]
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('drug_category', ['drug_id'=>'id']);
    }
    
    public function getImage(){
      return DataMocker::randomImageUrl();
    }

    /**
     * @return \yii\db\ActiveQuery|Stock[]
     */
    public function getStock()
    {
        return $this->hasMany(Stock::className(), ['drug_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery|DrugSubscription[]
     */
   public function getSubscriptions()
   {
       return $this->hasMany(DrugSubscription::className(), ['drug_id' => 'id']);
   }
}
