<?php

namespace app\models;

use Yii;

/**
 * @property Pharmacy $pharmacy
 * @property integer $pharmacy_id
 * */
class User extends \amnah\yii2\user\models\User
{
    public static function hasPharmacy()
    {
        if(($user = self::getCurrent()) == null)
            return $user;

        return $user->pharmacy_id != null;
    }

    /**
     * @return null|User
     */
    public static function getCurrent(){
        if(!Yii::$app->user->isGuest)
            return User::findOne(Yii::$app->user->id);
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery|Pharmacy|null
     */
    public function getPharmacy()
    {
        return $this->hasOne(Pharmacy::className(), ['id' => 'pharmacy_id']);
    }


}
