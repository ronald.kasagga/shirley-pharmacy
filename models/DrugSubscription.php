<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drug_subscription".
 *
 * @property integer $id
 * @property integer $drug_id
 * @property string $name
 * @property string $email
 *
 * @property Drug $drug
 */
class DrugSubscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drug_id'], 'integer'],
            [['email'], 'required'],
            [['name', 'email'], 'string', 'max' => 255],
            [['drug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drug::className(), 'targetAttribute' => ['drug_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'drug_id' => 'Drug ID',
            'name' => 'Name',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrug()
    {
        return $this->hasOne(Drug::className(), ['id' => 'drug_id']);
    }
}
