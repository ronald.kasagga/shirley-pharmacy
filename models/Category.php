<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $reference
 *
 * @property Drug[] $drugs
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'reference' => 'Reference',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery|Drug[]
     */
    public function getDrugs()
    {
        return $this->hasMany(Drug::className(), ['id' => 'drug_id'])
            ->viaTable('drug_category', ['category_id'=>'id']);
    }
}
