<?php

namespace app\models\search;

use app\components\Constants;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Stock as StockModel;

/**
 * Stock represents the model behind the search form about `app\models\Stock`.
 * @property Drug $drugSearch
 * @property Pharmacy $pharmacySearch
 */
class Stock extends StockModel
{
    public $drugSearch;
    public $pharmacySearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pharmacy_id', 'drug_id'], 'integer'],
            [['measure'], 'safe'],
            [['quantity', 'unit_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StockModel::find()->joinWith(['drug', 'pharmacy']);
        $this->drugSearch = new Drug();
        $this->pharmacySearch = new Pharmacy();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $this->drugSearch->load($params);
        $this->pharmacySearch->load($params);

        if(!in_array(null, [$this->pharmacySearch->latitude, $this->pharmacySearch->longitude]))
            $query->orderBy(
                "(111.1111 *
                   DEGREES(ACOS(COS(RADIANS({$this->pharmacySearch->latitude}))
                      * COS(RADIANS(latitude))
                      * COS(RADIANS({$this->pharmacySearch->longitude} - longitude))
                      + SIN(RADIANS({$this->pharmacySearch->latitude}))
                      * SIN(RADIANS(latitude))))) desc"
            );
        
        Constants::setConstant(Constants::SEARCHED_PHARMACY_COORDINATES, [
            'latitude'=>isset($_REQUEST['Pharmacy']['latitude']) ? $_REQUEST['Pharmacy']['latitude'] : null,
            'longitude'=>isset($_REQUEST['Pharmacy']['longitude']) ? $_REQUEST['Pharmacy']['longitude'] : null,
        ]);
            

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pharmacy_id' => $this->pharmacy_id,
            'drug_id' => $this->drug_id,
            'quantity' => $this->quantity,
            'unit_cost' => $this->unit_cost,
        ]);

        $query->andFilterWhere(['like', 'measure', $this->measure]);
        $query->andFilterWhere(['like', 'drug.name', $this->drugSearch->name]);

        $query->andFilterWhere(['like', 'pharmacy.name', $this->pharmacySearch->name]);

        return $dataProvider;
    }
}
