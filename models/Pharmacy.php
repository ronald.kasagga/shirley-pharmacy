<?php

namespace app\models;

use app\components\Calculator;
use app\components\Constants;
use Yii;

/**
 * This is the model class for table "pharmacy".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $location
 * @property string $latitude
 * @property string $longitude
 *
 * @property User[] $users
 * @property Stock[] $stock
 */
class Pharmacy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pharmacy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'location', 'latitude', 'longitude'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'location' => 'Location',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery|User[]
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['pharmacy_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Stock::deleteAll(['pharmacy_id'=>$this->id]);
            User::updateAll(['pharmacy_id'=>null], ['pharmacy_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function getStock()
    {
        return $this->hasMany(Stock::className(), ['pharmacy_id' => 'id']);
    }

    public function getDistance($latitude, $longitude, $units = 'km'){
        if(in_array(null, [$latitude, $longitude, $this->latitude, $this->longitude])) 
            return null;
        
        return Calculator::getDistance(
            ['latitude'=>$latitude, 'longitude'=>$longitude],
            ['latitude'=>$this->latitude, 'longitude'=>$this->longitude],
            $units
        );
    }

    public function getSearchedLocationDistance($units = 'km'){
        $coordinates = Constants::getConstant(Constants::SEARCHED_PHARMACY_COORDINATES);

        if($coordinates == null)
            return null;

        $distance = $this->getDistance($coordinates['latitude'], $coordinates['longitude'], $units);

        return $distance == null ? '' : "$distance $units";
    }
}
