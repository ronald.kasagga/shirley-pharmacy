<?php

namespace app\models;

use yii\base\Model;

/* @property string $name */
/* @property string $email */

class Subscriber extends Model
{
    public $name;
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email', 'name'], 'required'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }
}