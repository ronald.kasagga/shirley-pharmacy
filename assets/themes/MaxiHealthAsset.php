<?php

namespace app\assets\themes;

class MaxiHealthAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/themes/maxihealth/';

    public $depends = [
        'app\assets\AppAsset',
    ];
    
    public $css = [
        '//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic',
        '//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900',
        '//fonts.googleapis.com/css?family=Montserrat:400,700',
        '//fonts.googleapis.com/css?family=Philosopher:400,400italic,700,700italic',
        '//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic',
        //'web/libraries/bootstrap/bootstrap.min.css',
        'web/libraries/fonts/font-awesome.min.css',
        'web/libraries/owl-carousel/owl.carousel.css',
        'web/libraries/owl-carousel/owl.theme.css',
        'web/libraries/animate/animate.min.css',
        'web/libraries/magnific-popup/magnific-popup.css',
        'web/css/plugins.css',
        'web/css/navigation-menu.css',
        'web/style.css',
        'web/css/shortcodes.css',
    ];
    
    public $js = [
        'web/libraries/modernizr/modernizr.js',
        'web/js/jquery.easing.min.js',
        'web/libraries/appear/jquery.appear.js',
        'web/libraries/owl-carousel/owl.carousel.min.js',
        'web/libraries/number/jquery.animateNumber.min.js',
        'web/libraries/isotope/isotope.pkgd.min.js',
        'web/libraries/magnific-popup/jquery.magnific-popup.min.js',
        //'https://maps.googleapis.com/maps/api/js?v=3.exp',
        //'web/js/functions.js',
    ];
    
    public function header($siteName, $siteUrl, $navigation = []){
        return \Yii::$app->controller->renderFile("{$this->sourcePath}/views/header.php", [
            'siteName'=>$siteName,
            'siteUrl'=>$siteUrl,
            'navigation'=>$navigation,
            'theme'=>$this,
        ]);
    }
    
    public function footer($params){
        return \Yii::$app->controller->renderFile("{$this->sourcePath}/views/footer.php", [
            'theme'=>$this, 
            'params'=>$params,

        ]);
    }
}