<?php

namespace app\assets\plugins;

use yii\base\InvalidConfigException;
use yii\web\AssetBundle;
use yii\web\View;

class LocationPickerAsset extends AssetBundle
{
    public $sourcePath = '@app/plugins/location-picker/';

    public $js = [
        '//maps.google.com/maps/api/js?sensor=false&libraries=places',
        //'dist/locationpicker.jquery.js',
        'dist/locationpicker.jquery.min.js',
        'dist/locationpicker.jquery.min.js.map',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    /**
     * conf['mapDivId'] string defines the id of the div to which the map will be rendered
     * conf['initLocation']['latitude'] string defines the latitude of the initial map location
     * conf['initLocation']['longitude'] string defines the longitude of the initial map location
     * conf['latitudeInputId'] string defines id of the input field to which to write the latitude of the pinned point on the map
     * conf['longitudeInputId'] string defines id of the input field to which to write the longitude of the pinned point on the map
     * conf['locationInputId'] string defines id of the input field from which to listen for the location from the user
     * conf['onLocationChangedAction'] string javascript callback that is fired whenever a location changes. It's optional
     * conf['radiusInputId'] string defines id of the input field to which to write the bounding radius around of the pinned point on the map. It's Optional
     * conf['radius'] int defines the bounding radius around of the pinned point on the map. It's Optional
     * conf['zoom'] int defines the zoom level of the map. It's Optional
     *
     *
     * @param array $conf
     * @param View $view
     * @throws InvalidConfigException
     */
    public function initialize($conf = [], View $view){
        foreach (['mapDivId','initLocation','latitudeInputId','longitudeInputId','locationInputId',]
                 as $attribute)
            if(!isset($conf[$attribute]))
                throw new InvalidConfigException("The parameter '$attribute' is required in the '$conf' array!");

        if(!isset($conf['radius'])) $conf['radius'] = 0;
        if(!isset($conf['zoom'])) $conf['zoom'] = 15;
        if(!isset($conf['onLocationChangedAction'])) $conf['onLocationChangedAction'] =
            'function(currentLocation, radius, isMarkerDropped) {' ."\n".
            '       console.log("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");' ."\n" .
            '   }' ."\n";

        $js =
            "$('#{$conf['mapDivId']}').locationpicker({\n".
            "   location: {latitude: {$conf['initLocation']['latitude']}, longitude: {$conf['initLocation']['longitude']}},\n" .
            "    radius: {$conf['radius']},\n" .
            "    zoom: {$conf['zoom']},\n" .
            "    inputBinding: {\n" .
            "        latitudeInput: $('#{$conf['latitudeInputId']}'),\n" .
            "        longitudeInput: $('#{$conf['longitudeInputId']}'),\n" ;

        if(isset($conf['radiusInputId'])) $js .=
            "        radiusInput: $('#{$conf['radiusInputId']}'),\n";

        $js .=
            "        locationNameInput: $('#{$conf['locationInputId']}')\n" .
            "    },\n" .
            "    enableAutocomplete: true,\n" .
            "    onchanged: {$conf['onLocationChangedAction']}\n" .
            "});\n";

        $view->registerJs($js);
    }
}