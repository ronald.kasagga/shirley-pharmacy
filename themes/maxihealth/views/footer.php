<?php
/**
 * @var $theme \app\assets\themes\MaxiHealthAsset
 */
use yii\helpers\Html; ?>
<footer class="footer-main container-fluid no-padding">
    <!-- Container -->
    <div class="container">
        <!-- Contact Detail -->
        <div class="contact-details">
            <div class="col-md-4 col-sm-4 address-box detail-box">
                <i><img src="<?=$theme->baseUrl?>/web/images/ftr-location.png" alt="Loactaion" /></i>
                <h4>business address</h4>
                <p><?=Yii::$app->params['address']['place']?></p>
                <p><?=Yii::$app->params['address']['area']?></p>
            </div>
            <div class="col-md-4 col-sm-4 phone-box detail-box">
                <i><img src="<?=$theme->baseUrl?>/web/images/ftr-phone.png" alt="Phone" /></i>
                <h4>EMERGENCY ( 24X7 )</h4>
                <p>Mobile: <?=Yii::$app->params['contact']['phone']?></p>
                <p>Toll Free : (+1) 800 123 456</p>
            </div>
            <div class="col-md-4 col-sm-4 mail-box detail-box">
                <i><img src="<?=$theme->baseUrl?>/web/images/ftr-email.png" alt="Email" /></i>
                <h4>business contact</h4>
                <p><?=Html::mailto(Yii::$app->params['contact']['email'])?></p>
                <p><?=Html::mailto(Yii::$app->params['contact']['support'])?></p>
            </div>
        </diV><!-- Contact Detail /- -->

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <aside class="widget widget-about">
                    <h3><img src="<?=$theme->baseUrl?>/web/images/logo-ftr.png" alt="Logo" /></h3>
                        <p>Our service personnel are always available and eager to speak with you. Please reach out to
                        us via the contacts above within these hours</p>
                    <div class="time-schedule">
                        <p>Monday - Friday <span>8.00 - 18.00</span></p>
                        <p>Saturday <span>8.00 - 18.00</span></p>
                        <p>Sunday <span>8.00 - 13.00</span></p>
                    </div>
                </aside>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <aside class="widget widget-links">
                    <h3 class="widget-title">Drug Categories</h3>
                    <ul>
                        <?php foreach ($params['featuredCategories'] as $category):?>
                            <li><?=$category?></li>
                        <?php endforeach;?>
                    </ul>
                </aside>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <aside class="widget widget-newsletter">
                    <h3 class="widget-title">Newsletter</h3>
                    <p>Enter your email below to receive regular updates on the latest news, advancements, features on <?=Yii::$app->name?></p>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Email Address">
								<span class="input-group-btn">
									<button class="btn btn-default" type="button"><i class="fa fa-send-o"></i></button>
								</span>
                    </div><!-- /input-group -->
                    <div class="social">
                        <h6>Stay Connected</h6>
                        <ul>
                            <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div><!-- Container /- -->
    <!-- Bottom Footer -->
    <div class="container-fluid no-padding bottom-footer">
        <p>&copy; <?=date('Y')?> <?=Yii::$app->name?>. All Rights Reserved.</p>
    </div><!-- Bottom Footer /- -->
</footer>
