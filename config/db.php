<?php

$db = [
    'class' => 'yii\db\Connection',
    'charset' => 'utf8',
    'on afterOpen' => function($event) {
        // $event->sender refers to the DB connection
        $event->sender->createCommand("SET time_zone = '+3:00'")->execute();
    }
];

if(file_exists(__DIR__.'/db_local.php')){
    $dbConf = require_once __DIR__ .'/db_local.php';
    $db['dsn'] = $dbConf['dsn'];
    $db['username'] = $dbConf['username'];
    $db['password'] = $dbConf['password'];

}else if(isset($_ENV['OPENSHIFT_MYSQL_DIR'])){
    $host = $_ENV['OPENSHIFT_MYSQL_DB_HOST'];
    $port = $_ENV['OPENSHIFT_MYSQL_DB_PORT'];
    $dbName = $_ENV['OPENSHIFT_APP_NAME'];

    $db['dsn'] = "mysql:host=$host;port=$port;dbname=$dbName";
    $db['username'] = $_ENV['OPENSHIFT_MYSQL_DB_USERNAME'];
    $db['password'] = $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD'];
}else{
    $db['dsn'] = 'mysql:host=localhost;dbname=test';
    $db['username'] = 'root';
    $db['password'] = '';
}

return $db;
