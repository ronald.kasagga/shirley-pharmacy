<?php
$host = ( php_sapi_name() == 'cli' ) ? gethostname() : $_SERVER['SERVER_NAME'];
return [
    'server_name'=>$host,
    'adminEmail' => "shirley@$host",
    'business-email'=>"hello@$host",
    'address'=>[
        'place'=>'IUEA Kampala',
        'area'=>'Kansanga'
    ],
    'contact'=>[
        'phone'=>'(+256) 791 399981',
        'email'=>'nakalemagirl@gmail.com',
        'support'=>"support@$host",
    ]
];
